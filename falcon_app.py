    #!/usr/bin/env python
"""
Primary author: sunilavr
this the placeholder for all the routes served via falcon
using falcon middleware to secure the calls via auth
excluded ping for the sake of ALB health check
"""
import sys
import falcon

AUTH_TOKEN = 'xxxxxxxx'

class AuthMiddleware(object):
    """Flacon middleware for authenticating the requests"""

    def process_request(self, req, resp):
        """processing the falcon request and response"""
        token = req.get_header('X-App-Auth')
        global CURL_URI
        CURL_URI = req.uri

        if token is None and "/ping" not in CURL_URI:
            description = ('Please provide an auth token '
                           'as part of the request.')
            raise falcon.HTTPUnauthorized('Auth token required',
                                          description)

        if not self._token_is_valid(token):
            description = ('The provided auth token is not valid. '
                           'Please request a new token and try again.')
            raise falcon.HTTPUnauthorized('Authentication required',
                                          description)

    def _token_is_valid(self, token):
        "validating the token here"""
        if token == AUTH_TOKEN:
            return True
        if "/ping" in CURL_URI:
            return True
        else:
            return None


class Ping(object):
    """simple route for health check of the app"""

    def on_get(self, req, resp):
        "reading the response for health"""
        resp.body = 'SUCCESS!!'
        resp.status = falcon.HTTP_200

    def on_head(self, req, resp):
        """200OK for head"""
        resp.status = falcon.HTTP_200

#initialize the api with authentication
api = falcon.API(middleware=AuthMiddleware())

#all the routes for the apis go here 
api.add_route('/ping', Ping())
